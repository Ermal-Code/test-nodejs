# NodeJS Assigment

This test is designed to evaluate in general the candidates' back-end skills, such as understanding of RESTful APIs, principles of MongoDB, and basic NodeJS knowledge. It consists of a simple web app built on Express and relying on a MongoDB database. The expected total time to solve the assignments is 1 hour. Please read through the assignment descriptions before starting.

## Tasks

The purpose of this assignment complete the following tasks:

- Add comments to the code providing information on the functions and files involved;
- Following the RESTful pattern, create two new endpoints to perform the following functions:
  1. find a given user by mongo ID and update his data;
  2. find a given user by mongo ID and delete it.
- Add parameters validation for each endpoint that needs it;
- Create a fork of the project.

If you have time:

- Refactor one endpoint to use the async/await pattern;
- Add pagination to the "GET users" endpoint.

## Evaluation

We will evaluate you on the following parameters:

- Code understanding and comments quality;
- Ability to write code that is clean, readable and easy to maintain.
- Knowledge of NodeJS and involved patterns.

## Setup

Install node modules

```zsh
npm install
```

Run local site for development

```zsh
npm start
```
