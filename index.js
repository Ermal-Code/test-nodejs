import { connect } from 'mongoose'
// import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'
import q2m from 'query-to-mongo'

// We are using connect from mongoose to connect our app with the mongoDb database
connect(
  'mongodb+srv://test-user:17L2WY9bUWXqQmn3bnfPg8lB@maincluster.kwdmr.gcp.mongodb.net/yakkyofy?retryWrites=true&w=majority',
  { useNewUrlParser: true }
)

// Here we are creating an instance of express
const app = express()

//Here we are using cors middleware which is use for security. What cors does is telling the server which app can connect
// with this serve and API but since cors has no opptions parameter will allow all to connect with this API this is usally done when working with localhost
app.use(cors())

// My editor was telling me that bodyParser is depricated so i used express.json() instead for parsing request to json.So this app will only parse JSON type objects.

// app.use(bodyParser.urlencoded({ extended: true }))
// app.use(bodyParser.json())
app.use(express.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

// Here we created an endpoint which will return users that are inside this database and number of links.Depending if the limit parameter is added this endpoint
// will return users depending on the limit paramater value. I used query-to-mongo package which gives some good options for different query parameters.
// In this case I am using it for pagination where skip option will allow to skip the number of documents that it recieves from req.query.skip and also limit which will limit the number
// of documents returned based on req.query.limit. Also I am using countDocuments so i count all documents inside database. And on response I am using links method which in combination with total and
// depending on the limit query parameter will show how in how many links is the database seperated and will give the front end developer a clearer view on how to display them.

app.get('/users', async (req, res) => {
  try {
    const query = q2m(req.query)
    const total = await UserModel.countDocuments(query.criteria)

    const users = await UserModel.find(query.criteria, query.options.fields)
      .skip(query.options.skip)
      .limit(query.options.limit)

    res.status(200).send({ links: query.links('/users', total), users })
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
  }
})

// This endpoint is used for creating a new user based on the User Model. Which is imported from UserModel.js file. If the user is created successfully it will return a response with
// status 201 and the created user. Otherwise it will send as response errorrs.
app.post('/users', async (req, res) => {
  try {
    let user = new UserModel()

    user.email = req.body.email
    user.firstName = req.body.firstName
    user.lastName = req.body.lastName
    user.password = req.body.password

    await user.save()

    res.status(201).send(user)
  } catch (error) {
    res.status(500).send(error)
  }
})

// This endpoint is used for editing user info. Using findByIdAndUpdate we will find user based on Id that is provided by params. Than as a second
// parameter we will provide the request body which will change the info based on that. As a third parameter we added paramater validation.
// If the id provided is wrong it will throw a 404 error telling the user that it can't find the user with the id provided.
app.put('/users/:userId', async (req, res) => {
  try {
    const updatedUser = await UserModel.findByIdAndUpdate(req.params.userId, req.body, {
      runValidators: true,
      new: true,
    })

    if (updatedUser) {
      res.status(200).send(updatedUser)
    } else {
      const err = new Error()
      err.message = `User with id: ${req.params.userId} not found!`
      err.httpStatusCode = 404
      res.status(404).send(err)
    }
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
  }
})

// We use this endpoint to delete a user using findByIdAndDelete and adding as a parameter the user id provided in params.This function will use that id to
// search database and find User with that mongo ID if found the user will be deleted else it will throw a 404 error telling that it could not find a user in
// database with the provided id
app.delete(`/users/:userId`, async (req, res) => {
  try {
    const deletedUser = await UserModel.findByIdAndDelete(req.params.userId)
    if (deletedUser) {
      res.status(203).send({ response: `User with id:${req.params.userId} is deleted` })
    } else {
      const err = new Error()
      err.message = `User with id: ${req.params.userId} not found!`
      err.httpStatusCode = 404
      res.status(404).send(err)
    }
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
  }
})

// listen method is used to listen on connection on specified host and port
app.listen(8080, () => console.log('Example app listening on port 8080!'))
